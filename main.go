package main

import "gitlab.com/aceptius-public/kiwix-zim-updater/cmd"

func main() {
	cmd.Execute()
}
