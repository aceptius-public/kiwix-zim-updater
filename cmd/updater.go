package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	ar "gitlab.com/aceptius-public/apache-directory-index-reader"
)

// updaterCmd represents the updater command
var updaterCmd = &cobra.Command{
	Use:   "updater",
	Short: "",
	Long: `
		Environments:
         KWX_SOURCE_URL - url to source (e.g https://ftp.fau.de/kiwix/zim)
	`,
	Run: func(cmd *cobra.Command, args []string) {

		ctx := context.Background()

		reader := ar.DefaultReader()
		list, err := reader.GetList(ctx, "https://ftp.fau.de/kiwix/zim/wikipedia/")
		if err != nil {
			panic(err)
		}
		fmt.Printf("%+v", list)

	},
}

func init() {
	rootCmd.AddCommand(updaterCmd)
}
