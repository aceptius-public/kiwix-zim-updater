FROM golang:1.19 as build

COPY . /opt/project/
WORKDIR /opt/project

RUN CGO_ENABLED=0 go build -ldflags="-s -w" -o /zim-updater

FROM scratch
COPY --from=build /zim-updater /zim-updater
ENTRYPOINT ["/zim-updater"]