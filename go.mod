module gitlab.com/aceptius-public/kiwix-zim-updater

go 1.19

require (
	github.com/spf13/cobra v1.6.1
	gitlab.com/aceptius-public/apache-directory-index-reader v0.0.0-20221122144650-89b4e9e2b484
)

require (
	github.com/PuerkitoBio/goquery v1.8.0 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8 // indirect
)
